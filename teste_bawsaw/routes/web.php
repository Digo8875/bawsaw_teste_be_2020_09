<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Base
Route::get('/exemplo', 'SistemaController@exemplo')->name('exemplo');

Route::get('/cadastrar_usuario', 'SistemaController@cadastrar_usuario')->name('cadastrar_usuario');

Route::get('/listar_usuarios', 'SistemaController@listar_usuarios')->name('listar_usuarios');

Route::post('/usuarios_do_quadrante', 'SistemaController@usuarios_do_quadrante')->name('usuarios_do_quadrante');

//Extra
Route::get('/ajax_sort_usuario', 'SistemaController@ajax_sort_usuario')->name('ajax_sort_usuario');

Route::get('/excluir_usuario', 'SistemaController@excluir_usuario')->name('excluir_usuario');

Route::get('/atualizar_usuario', 'SistemaController@atualizar_usuario')->name('atualizar_usuario');

Route::get('/mapa', 'SistemaController@mapa')->name('mapa');