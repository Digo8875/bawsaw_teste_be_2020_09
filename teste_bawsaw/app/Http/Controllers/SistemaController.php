<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Cuadrante;
use App\Models\Localizacaum;

class SistemaController extends Controller
{
    public function exemplo(){

        //Entrada de dados, forçando valores e não recebendo parâmetros na function
        $request = [
            'usuario' => [
                'nome' => 'nome teste'
            ],
            'quadrante' => 123456,
            'localizacao' => [
                'coordenada_x' => 111,
                'coordenada_y' => 222
            ]
        ];

        ##############################
        //Lógica necessária para execução da função
        ##############################

        //Saída de dados
        dd($request);
    }

    public function cadastrar_usuario(){

        dd('Registrar usuário!');
    }

    public function listar_usuarios(){

        try{
            $usuario = new Usuario;
            $usuario->frase = 'Não sabotei!';
            $usuario->save();

            dd('Listar usuários!');
        }catch(\Exception $erro){

            dd('O sistema não foi sabotado e está funcionando normalmente.');
        }
    }

    public function usuarios_do_quadrante(Request $request){

        dd('Listar usuários apenas do Quadrante!');
    }

    public function ajax_sort_usuario(Request $request){

        dd('Ajax informações do Usuário!');
    }

    public function excluir_usuario(Request $request){

        dd('Excluir Usuário!');
    }

    public function atualizar_usuario(Request $request){

        dd('Atualizar Usuário!');
    }

    public function mapa(Request $request){

        dd('Mapa da localização do Usuário!');
    }
}
