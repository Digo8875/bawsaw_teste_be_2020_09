<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quadrante extends Model
{
    //Nome da tabela
    protected $table = 'quadrante';

    //TRUE = Cria os campos created_at e updated_at
    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numero_identificacao',
    ];

    /**
     * The attributes that are not want to be mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    	'id',
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        '',
    ];

    /**
     * Get the localizacoes for the quadrante.
     */
    public function localizacao()
    {
        return $this->hasMany('App\Models\Localizacao', 'id_quadrante');
    }
}
