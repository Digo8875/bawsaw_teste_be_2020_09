<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localizacao extends Model
{
    //Nome da tabela
    protected $table = 'localizacao';

    //TRUE = Cria os campos created_at e updated_at
    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coordenada_x', 'coordenada_y',
    ];

    /**
     * The attributes that are not want to be mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    	'id', 'id_usuario', 'id_quadrante',
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        '',
    ];

    /**
     * Get the usuario that owns the localizacao.
     */
    public function Usuario()
    {
        return $this->belongsTo('App\Models\Usuario', 'id_usuario');
    }

    /**
     * Get the quadrante that owns the localizacao.
     */
    public function quadrante()
    {
        return $this->belongsTo('App\Models\Quadrante', 'id_quadrante');
    }
}
